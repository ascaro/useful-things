# Useful-Links

#1  
[color.adobe.com](https://color.adobe.com/)  
Neat color picker.

#2  
[waifu2x.udp.jp](http://waifu2x.udp.jp/index.html)  
Single-Image Super-Resolution for Anime-Style Art using Deep Convolutional Neural Networks. And it supports photo.

#3  
[stocksnap.io](https://stocksnap.io/)  
Beautiful free stock photos. Hundreds of high resolution images added weekly. Free from copyright restrictions.

#4  
[choosealicense.com](http://choosealicense.com/licenses/mit/)  
Choose an open source license.

#5  
[colorsafe.co](http://colorsafe.co/)  
Empowering designers with beautiful and accessible color palettes based on WCAG Guidelines of text and background contrast ratios.

#6  
[fontawesome.io](http://fontawesome.io/)  
The Icons. The complete set of 634 icons in Font Awesome 4.6.3

#7  
[css2sass.herokuapp.com](http://css2sass.herokuapp.com/)    
CSS 2 SASS/SCSS CONVERTER

#8  
[www.fakenamegenerator.com](http://www.fakenamegenerator.com/)  
Generate a random identity.

#9  
[digitalocean.com/community/tutorials](https://www.digitalocean.com/community/tutorials/)  
Follow along with one of our 1438 development and sysadmin tutorials.  
